# Begone Wanderers

A mod for [RimWorld](http://rimworldgame.com/).

## Description

Sets the probability of the "Wanderer Joins" incident to zero.

## Compatibility

* Rimworld Version: 0.15.1284
* Overwrites the existing "Wanderer Joins" incident, so must be loaded after Core and may affect any other mod that alters it or incidents in general
